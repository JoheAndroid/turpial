  <?php 
  $servicios = '[
    {
      "0": "1",
      "id_fruta": "1",
      "1": "Diseño web",
      "titulo": "Diseño web",
      "description": "El diseño web abarca muchas habilidades y disciplinas diferentes en la producción y mantenimiento de sitios web. Las diferentes áreas del diseño web incluyen diseño gráfico web, diseño de interfaz, autoría, incluido código estandarizado.",
      "imagen": "img/undraw_design_notes_8dmv.svg",
      "2": "100",
      "cantidad": "100"
    },
    {
      "0": "2",
      "id_fruta": "2",
      "1": "Desarrollo web",
      "titulo": "Desarrollo web",
      "description": "Diseñamos soluciones en digital a todo lo que se pueda imaginar, dandole vida a tus proyectos que siempre quisiste tener, usamos las mejores tecnología mas avanzadas para obtener un producto de alta gama y gran competencia en el mercado actual",
      "imagen": "img/undraw_web_developer_p3e5.svg",
      "2": "167",
      "cantidad": "167"
    },
    {
      "0": "3",
      "id_fruta": "3",
      "1": "Pera",
      "titulo": "Marketing",
      "description": "Aplicación de técnicas de marketing digital en redes sociales, buscadores y páginas web mediante Facebook Ads y Google Ads para aumentar tus conversiones.",
      "imagen": "img/undraw_mobile_marketing_iqbr.svg",
      "2": "820",
      "cantidad": "820"
    }
  ]';

$array = json_decode($servicios);
