<div class="section bg-light-2">
            <section class="section section-demo section_w-container-left text-left">
              <div class="container-fluid section-demo__container-right">
                <div class="row align-items-center">
                  <div class="col-lg-6 offset-lg-6">
                    <div class="section-demo__content pt-medium pb-medium">
                      <section class="section section-content clearfix" data-arts-os-animation="data-arts-os-animation">
                        <div class="section-content__inner">
                          <div class="w-100"></div>
                          <div class="section__headline mb-1 mb-md-2 mr-auto"></div>
                          <div class="w-100"></div>
                          <div class="section-content__heading split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">
                            <h2>EXPERIENCIA</h2>
                          </div>
                          <div class="w-100"></div>
                          <div class="section-content__text split-text js-split-text mt-1" data-split-text-type="lines" data-split-text-set="lines">
                            <p>Desarrollamos soluciones creativas para pequeñas y grandes marcas por igual, construimos identidades de productos auténticos y mucho más.</p>
                          </div>
                        </div>
                      </section>
                      <aside class="aside aside-counters section">
                        <div class="container no-gutters">
                          <div class="row justify-content-center">
                            <!-- counter -->
                            <div class="aside-counters__wrapper-item col-6 mt-small">
                              <div class="counter js-counter" data-counter-start="0" data-counter-target="28" data-counter-duration="4">
                                <!-- animated number -->
                                <div class="counter__number js-counter__number">28</div>
                                <!-- - animated number -->
                                <div class="counter__label">Proyectos realizados</div>
                              </div>
                            </div>
                            <!-- - counter -->
                            <!-- counter -->
                            <div class="aside-counters__wrapper-item col-6 mt-small">
                              <div class="counter js-counter" data-counter-start="0" data-counter-target="15" data-counter-duration="4">
                                <!-- animated number -->
                                <div class="counter__number js-counter__number">15</div>
                                <!-- - animated number -->
                                <div class="counter__label">Campañas</div>
                              </div>
                            </div>
                            <!-- - counter -->
                          </div>
                        </div>
                      </aside>
                      <div class="mt-small d-lg-none"><a class="button button_bordered button_black" data-hover="Explore Pages" href="page-inner-services-1.html" target="_blank"><span class="button__label-hover">Explorar más</span></a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <div class="section-demo__bg section-demo__bg_wide section-demo__bg_left bg-white" data-arts-parallax data-arts-parallax-factor="0.3">
              <div>
                <div class="lazy-bg" data-src="img/assets/sectionDemo/inner-pages-screen-1.png"></div>
              </div>
            </div>
            <div class="section-demo__wrapper-button section-demo__wrapper-button_left d-none d-lg-flex"><a class="circle-button circle-button_link js-circle-button" data-arts-cursor="data-arts-cursor" data-arts-cursor-hide-native="true" data-arts-cursor-scale="0" data-arts-cursor-magnetic="data-arts-cursor-magnetic" data-arts-cursor-icon="add" href="page-inner-services-1.html" target="_blank" data-arts-os-animation="true">
                <!-- curved label -->
                <div class="circle-button__outer">
                  <div class="circle-button__wrapper-label">
                    <div class="circle-button__label small-caps">Explore Pages</div>
                  </div>
                </div>
                <!-- - curved label -->
                <!-- geometry wrapper -->
                <div class="circle-button__inner">
                  <div class="circle-button__circle">
                    <svg class="svg-circle" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <circle class="circle" cx="30" cy="30" r="29" fill="none"></circle>
                    </svg>
                  </div>
                  <!-- browsers with touch support -->
                  <div class="circle-button__icon circle-button__icon-touch">
                    <div class="material-icons">add</div>
                  </div>
                  <!-- - browsers with touch support -->
                </div>
                <!-- - geometry wrapper --></a>
            </div>
          </div>