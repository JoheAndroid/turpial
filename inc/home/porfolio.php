
<?php 
  require('inc/db.php');
  $portafolio = '[
    {
      "0": "1",
      "id_fruta": "1",
      "nombre": "Veriun",
      "titulo": "Diseño web",
      "description": "El diseño web abarca muchas habilidades y disciplinas diferentes en la producción y mantenimiento de sitios web. Las diferentes áreas del diseño web incluyen diseño gráfico web, diseño de interfaz, autoría, incluido código estandarizado.",
      "imagen": "https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/87d9b489864351.5e03eaf8de32b.png",
      "2": "100",
      "cantidad": "100",
      "link": "portafolio/1"
    },
    {
      "0": "2",
      "id_fruta": "2",
      "nombre": "Cajitas",
      "titulo": "Desarrollo web",
      "description": "Diseñamos soluciones en digital a todo lo que se pueda imaginar, dandole vida a tus proyectos que siempre quisiste tener, usamos las mejores tecnología mas avanzadas para obtener un producto de alta gama y gran competencia en el mercado actual",
      "imagen": "https://mir-cdn.behance.net/v1/rendition/project_modules/fs/e95dec89901523.5e0560b3dc25e.png",
      "2": "167",
      "cantidad": "167",
      "link": "project/puntodata"
    },
    {
      "0": "3",
      "id_fruta": "3",
      "nombre": "Puntodata",
      "titulo": "Marketing",
      "description": "Aplicación de técnicas de marketing digital en redes sociales, buscadores y páginas web mediante Facebook Ads y Google Ads para aumentar tus conversiones.",
      "imagen": "https://mir-s3-cdn-cf.behance.net/project_modules/fs/623e6a89917069.5e05fec37dd8e.png",
      "2": "820",
      "cantidad": "820",
      "link": "project/puntodata"
    },
    {
      "0": "4",
      "id_fruta": "4",
      "nombre": "Little Piazza",
      "titulo": "Marketing",
      "description": "Aplicación de técnicas de marketing digital en redes sociales, buscadores y páginas web mediante Facebook Ads y Google Ads para aumentar tus conversiones.",
      "imagen": "https://mir-s3-cdn-cf.behance.net/project_modules/fs/2b384889877563.5e0490f741258.png",
      "2": "820",
      "cantidad": "820",
      "link": "project/puntodata"
    },
    {
      "0": "5",
      "id_fruta": "4",
      "nombre": "Accud",
      "titulo": "Desarrollo web y Diseño web",
      "description": "Maquinarias y Herramientas",
      "imagen": "https://ucarecdn.com/163d5353-d65d-4003-8893-5e067b0b4c12/Screenshot1.png",
      "2": "820",
      "cantidad": "820",
      "link": "project/puntodata"
    },
    {
      "0": "5",
      "id_fruta": "4",
      "nombre": "Valdevellano Asesorías",
      "titulo": "Desarrollo web y Diseño web",
      "description": "tiene como misión brindar seguridad y metodologías de prevención de riesgos a nuestros colaboradores con el fin de desarrollar sus labores en un ambiente de trabajo",
      "imagen": "https://ucarecdn.com/548a09f7-bf28-47d9-bb5b-e04e22c7cbe5/Screenshot2.png",
      "2": "820",
      "cantidad": "820",
      "link": "project/puntodata"
    }
  ]';

  $porta = json_decode($portafolio);

?>
      <section class="section section-masthead d-none" data-background-color="var(--color-light-1)"></section>
          <!-- - section MASTHEAD -->
          <!-- section GRID IRREGULAR 2 -->
          <section class="section section-grid section-content overflow bg-light-1 pt-large pb-medium" data-arts-os-animation="data-arts-os-animation" data-grid-columns="2" data-grid-tablet="1" data-grid-mobile="1">
            <div class="container-fluid">
              <div class="row justify-content-between align-items-center pb-medium">
                <div class="col-12 col-lg-auto">
                  <div class="section-grid__heading split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">
                    <h1 class="h1 mt-0 mb-0">Works</h1>
                  </div>
                  <div class="w-100"></div>
                  <div class="section__headline mt-2 mr-auto"></div>
                </div>
                <div class="col-12 col-lg-auto pt-medium pt-md-0 pb-md-3">
                  <div class="filter js-filter">
                    <div class="filter__inner">
                      <div class="container-fluid no-gutters">
                        <!-- items -->
                        <div class="row justify-content-center">
                          <!-- all (*) -->
                          <div class="col-lg-auto col-12 filter__item filter__item_active js-filter__item" data-filter="*">
                            <div class="split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">All</div>
                          </div>
                          <!-- - all (*) -->
                          <?php
                          $query = "SELECT * FROM categories";      
                          if ($result = $mysqli->query($query)) {
                
                            while ($row = $result->fetch_assoc()) {  ?>
                          <div class="col-lg-auto col-12 filter__item js-filter__item" data-filter=".<?php echo $row["name"]; ?>">
                            <div class="split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words"><?php echo $row["name"]; ?></div>
                          </div>
                          <?php  }

                          }
                          
                          
                          ?>
                          
                          <!---<div class="col-lg-auto col-12 filter__item js-filter__item" data-filter=".category-branding">
                            <div class="split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">Branding</div>
                          </div>
                          <div class="col-lg-auto col-12 filter__item js-filter__item" data-filter=".category-identity">
                            <div class="split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">Identity</div>
                          </div>
                          <div class="col-lg-auto col-12 filter__item js-filter__item" data-filter=".category-photography">
                            <div class="split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">Photography</div>
                          </div>-->
                          <!-- - items-->
                        </div>
                        <!-- underline -->
                        <div class="filter__underline js-filter__underline"></div>
                        <!-- - underline -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="grid grid_fluid-4 js-grid">
                <div class="grid__item grid__item_desktop-6 grid__item_tablet-12 grid__item_mobile-12 grid__item_fluid-4 grid__sizer js-grid__sizer"></div>
                <!---->
                <?php
          $query = "SELECT * FROM porfolio";      
          if ($result = $mysqli->query($query)) {

            while ($row = $result->fetch_assoc()) {
                $titulo= $row["titulo"];
                $imagen= $row["imagen"];
                $description = $row['description'];
                $rol= $row["rol"];
                $id= $row["id"];
                $query2 = "SELECT * FROM categories WHERE id = '$id'";    
                $categoriesName;
                if ($result2 = $mysqli->query($query2)) {
                  while ($row2 = $result2->fetch_assoc()) {
                    $categoriesName = $row2["name"];
                  }
                }
                ?>
                <div class="grid__item grid__item_desktop-6 grid__item_tablet-12 grid__item_mobile-12 grid__item_fluid-4 js-grid__item <?php echo $categoriesName; ?>">
                  <div class="section-grid__item">
                    <a title="<?php echo $rol; ?>"  alt="<?php echo $rol; ?>" class="figure-project hover-zoom js-change-text-hover" href="portafolio/<?php echo $id; ?>" data-pjax-link="flyingImage">
                      <div class="hover-zoom__inner">
                        <div class="hover-zoom__zoom">
                          <div class="figure-project__wrapper-img js-transition-img" data-arts-parallax="data-arts-parallax" data-arts-parallax-factor="0.15">
                            <div class="lazy js-transition-img__transformed-el">
                              <img  title="<?php echo $description; ?>"  alt="<?php echo $description; ?>" data-src="<?php echo $imagen; ?>" src="#" width="1920" height="1920">
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="figure-project__content pl-md-2 pt-md-2 pt-1 pl-0">
                        <h2 class="h3 figure-project__heading"><?php echo $titulo; ?></h2>
                        <div class="figure-project__category mt-md-1 mt-0-5">
                          <div class="change-text-hover small-caps js-change-text-hover change-text-hover_has-line text-left">
                            <!-- label by default -->
                            <div class="change-text-hover__normal js-split-text split-text js-change-text-hover__normal" data-split-text-type="lines" data-split-text-set="lines"><?php echo $rol; ?></div>
                            <!-- - label by default -->
                            <!-- label on hover -->
                            <div class="change-text-hover__hover js-change-text-hover__hover">
                              <!-- hover line -->
                              <div class="change-text-hover__line js-change-text-hover__line"></div>
                              <!-- - hover line --><span class="js-split-text split-text" data-split-text-type="lines" data-split-text-set="lines">Explorar proyecto</span>
                            </div>
                            <!-- - label on hover -->
                          </div>
                        </div>
                      </div></a>
                  </div>
                </div>
                <!---->
          <?php  }
        } ?>
              </div>
            </div>
          </section>