
                            

<footer class="footer container-fluid" id="page-footer" data-arts-theme-text="dark" data-arts-footer-logo="primary">
          <!-- widgets top area -->
          <div class="footer__area pt-md-5 pt-sm-3 pb-md-3 pb-sm-1 pt-2 pb-0 footer__area-border-top">
            <div class="row">
              <!-- call to action -->
              <div class="col-lg-5">
                <section class="widget widget_rhye_cta">
                  <!-- header -->
                  <h2 class="h2 mt-0 mb-0-5">¿Listo para capturar los momentos?</h2>
                  <!-- - header -->
                  <!-- button -->
                  <a class="button button_solid button_black mb-0-5" data-hover="Get in Touch" href="page-inner-contacts-1.html"><span class="button__label-hover">Get in Touch</span></a>
                  <!-- - button -->
                </section>
              </div>
              <!-- - call to action -->
              <div class="col-lg-7">
                <div class="row ">
                  <!-- widget MENU #1 -->
                  <?php if (false){ ?>
                  <div class="col-lg-3 col-sm-6 col-12">
                    <section class="widget widget_nav_menu">
                      <!-- header -->
                      <h2 class="widgettitle"><?php echo date("Y"); ?>' Works</h2>
                      <!-- - header -->
                      <!-- content -->
                      <ul class="menu js-menu">
                        <li><a href="project-details-1-fullscreen-center.html">Nordic Adventure</a>
                        </li>
                        <li><a href="project-details-2-bottom-center.html">Sunrise in Desert</a>
                        </li>
                        <li><a href="project-details-3-halfscreen-left-right.html">Cassio Apartment</a>
                        </li>
                        <li><a href="project-details-4-halfscreen-left-left.html">Nothern Wave</a>
                        </li>
                        <li><a href="project-details-5-bottom-container-center.html">On the Edge</a>
                        </li>
                        <li><a href="project-details-6-bottom-fullwidth.html">Paysage Fiction</a>
                        </li>
                        <li><a href="project-details-7-fullscreen-left.html">Silence &amp; Noise</a>
                        </li>
                      </ul>
                      <!-- - content -->
                    </section>
                  </div>
                  <!-- - widget MENU #1 -->
                  <!-- widget MENU #2 -->
                  <div class="col-lg-3 col-sm-6 col-12">
                    <section class="widget widget_nav_menu">
                      <!-- header -->
                      <h2 class="widgettitle">2019' Works</h2>
                      <!-- - header -->
                      <!-- content -->
                      <ul class="menu js-menu">
                        <li><a href="project-details-8-halfscreen-right-left.html">Minimalex Cosmetics</a>
                        </li>
                        <li><a href="project-details-9-fullscreen-center.html">La Isla Bonita</a>
                        </li>
                        <li><a href="project-details-10-halfscreen-left-right.html">Kinsey Premium Furniture</a>
                        </li>
                        <li><a href="project-details-11-fullscreen-center.html">Dancing in the Dark</a>
                        </li>
                        <li><a href="project-details-12-bottom-container-center.html">The Silent Listener</a>
                        </li>
                        <li><a href="project-details-13-bottom-container-center.html">Arrigo Casual Accessories</a>
                        </li>
                      </ul>
                      <!-- - content -->
                    </section>
                  </div>
                  <?php } ?>
                  <!-- - widget MENU #2 -->
                  <!-- widget MENU #3 -->
                  <div class="col-lg-4 col-sm-6 col-12">
                    <section class="widget widget_nav_menu">
                      <!-- header -->
                      <h2 class="widgettitle">Servicios</h2>
                      <!-- - header -->
                      <!-- content -->
                      <ul class="menu js-menu">
                        <li><a href="#">Diseño web</a>
                        </li>
                        <li><a href="#">Progrmación Web</a>
                        </li>
                        <li><a href="#">SEO</a>
                        </li>
                        <li><a href="#">Marketing</a>
                        </li>
                        <li><a href="#">Branding</a>
                        </li>
                        
                      </ul>
                      <!-- - content -->
                    </section>
                  </div>
                  <!-- - widget MENU #3 -->
                  <!-- widget MENU #4 -->
                  <div class="col-lg-4 col-sm-3 col-12">
                    <section class="widget widget_nav_menu">
                      <!-- header -->
                      <h2 class="widgettitle">Contacto</h2>
                      <!-- - header -->
                      <!-- content -->
                      <ul class="menu js-menu">
                        <li><a href="page-inner-services-1.html">contacto@turpial.com</a>
                        </li>
                        <li><a href="page-inner-about-1.html">+56 9 3916 9312</a>
                        </li>
                        <li><a href="blog-grid-2-columns.html">Blog</a>
                        </li>
                        <li><a href="page-inner-contacts-1.html">Contacts</a>
                        </li>
                      </ul>
                      <!-- - content -->
                    </section>
                  </div>
                  <!-- - widget MENU #4 -->
                </div>
              </div>
            </div>
          </div>
          <!-- - widgets top area -->
          <!-- widgets bottom area -->
          <div class="footer__area footer__area-border-top pt-sm-2 pb-sm-1 pt-2 pb-0">
            <div class="row align-items-center">
              <!-- widget LOGO -->
              <div class="col-lg-3 footer__column text-center text-lg-left order-lg-1">
                <section class="widget widget_rhye_logo">
                  <!-- content --><a class="logo" href="index.html" target="_blank">
                  <div class="logo__wrapper-img">
                  <img class="" src="img/bird.png" style="height: 50px;" alt="Rhye HTML5 Template" height="20">
                  <!-- secondary logo version (for dark backgrounds)--><img class="logo__img-secondary" src="img/Turpial-01.png" alt="Rhye HTML5 Template" height="20">
                </div></a>
                  <!-- - content -->
                </section>
              </div>
              <!-- - widget LOGO -->
              <!-- widget SOCIAL -->
              <div class="col-lg-3 footer__column text-center text-lg-right order-lg-3">
                <section class="widget widget_rhye_social">
                  <!-- content -->
                  <ul class="social">
                    <li class="social__item"><a class="fa fa-facebook-f" href="#"></a></li>
                    <li class="social__item"><a class="fa fa-twitter" href="#"></a></li>
                    <li class="social__item"><a class="fa fa-instagram" href="#"></a></li>
                    <li class="social__item"><a class="fa fa-behance" href="#"></a></li>
                  </ul>
                  <!-- - content -->
                </section>
              </div>
              <!-- - widget SOCIAL -->
              <!-- widget TEXT -->
              <div class="col-lg-6 footer__column text-center text-lg-center order-lg-2">
                <section class="widget widget_text">
                  <!-- content -->
                  <div class="textwidget">
                    <p><small>© 2020 Turpial. Crafted by </> </p>
                  </div>
                  <!-- - content -->
                </section>
              </div>
              <!-- - widget TEXT -->
            </div>
          </div>
          <!-- - widgetst bottom area -->
        </footer>