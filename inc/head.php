
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Turpials Studio - Creative Developers</title>
    <?php #<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> ?>
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta content="telephone=no" name="format-detection">
    <meta name="HandheldFriendly" content="true">



        
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <meta name="google-site-verification" content="" />
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="google-signin-client_id" content="">

    <meta name="description" content="Somos una agencia digital, que crea proyectos web, sociales y de comunicación estratégica centrada en el desarrollo, diseño y marketing, ayudamos a marcas y empresas a destacar en los medios digitales." />
    <meta name="keywords" content="diseño, web" />
    <meta name="author" content="Turpials" />
    <meta name="copyright" content="turpialstudio.com" />
    <meta name="application-name" content="Turpials" />
    <meta name="mobile-web-app-capable" content="yes">
                <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="article">
    <meta property="og:locale" content="es-CL">
    <meta property="og:title" content="Turpial - Diseño web | Bigbuda | Marketing Digital - Branding - Redes Sociales - Las Condes, Vitacura, Providencia y Santiago">
    <meta property="og:url" content="<?php echo $_SERVER['HTTP_HOST'] ?>">
    <meta property="og:image"  content="<?php echo $_SERVER['HTTP_HOST'] ?>/img/bird.png">
    <meta property="og:site_name" content="Turpials">
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image:width" content="200">
    <meta property="og:image:height" content="200">
    <meta property="og:image:url" content="<?php echo $_SERVER['HTTP_HOST'] ?>/img/bird.png" />
    <meta property="og:image:secure_url" content="<?php echo $_SERVER['HTTP_HOST'] ?>/img/bird.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:description" content="Turpial - Diseño web | Bigbuda | Marketing Digital - Branding - Redes Sociales - Las Condes, Vitacura, Providencia y Santiago">
                            <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="<?php echo $_SERVER['HTTP_HOST'] ?>">
    <meta name="twitter:title" content="Turpial - Diseño web | Bigbuda | Marketing Digital - Branding - Redes Sociales - Las Condes, Vitacura, Providencia y Santiago">
    <meta name="twitter:url" content="<?php echo $_SERVER['HTTP_HOST'] ?>">
    <meta name="twitter:description" content="Turpial - Diseño web | Bigbuda | Marketing Digital - Branding - Redes Sociales - Las Condes, Vitacura, Providencia y Santiago">
    <meta name="twitter:image" content="<?php echo $_SERVER['HTTP_HOST'] ?>/img/bird.png">
    <meta name="twitter:creator" content="@turpialStudio">
    <meta name="twitter:domain" content="<?php echo $_SERVER['HTTP_HOST']; ?>">
    <meta name="robots" content="index, follow">

    <link rel="canonical" href="<?php echo $_SERVER['HTTP_HOST']; ?>">



   <?php include 'head-style.php'; ?>
    <!-- Favicons & App Icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/content/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/content/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/content/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/content/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/content/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/content/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/content/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/content/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/content/apple-icon-180x180.png">
  
    <link rel="icon" type="image/png" sizes="32x32" href="img/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icon/favicon-16x16.png">
    <link rel="manifest" href="img/icon/site.webmanifest">
    <link rel="manifest" href="img/content/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/content/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  </head>