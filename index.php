<?php 
function truncate($text, $chars = 25) {
    if (strlen($text) <= $chars) {
        return $text;
    }
    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    $text = $text."...";
    return $text;
}
$preload = true;
$sliders = true;
$team = false;
?>
<!DOCTYPE html>
<html  lang="es-CL"  prefix="og: http://ogp.me/ns# "  class="home">
<?php
  include 'inc/porfolio.php';
?>
<head>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>

   <?php include 'inc/head.php'; ?>
  <body>
   


    
    <div data-barba="wrapper" id="main-content">
      <!-- PAGE PRELOADER -->
      <?php 
      if ($preload) {  ?>
        <div class="preloader text-center bg-dark-2" id="js-preloader" data-arts-theme-text="light">
        <div class="noisa"></div>
        <div class="preloader__content">
          <!-- header -->
          <div class="preloader__header mt-auto">
            <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_wlSAOz.json"  background="transparent"  speed="1"  style="width: 250px; height: 250px;"  loop  autoplay></lottie-player>
            <div class="preloader__heading h2" style="margin-bottom:10px">Turpial Studios</div>
            <div class="preloader__subline small-caps mt-1">Creative Developer</div>
          </div>
          <!-- - header -->
          <!-- counter -->
          <div class="preloader__counter h5"><span class="preloader__counter-number preloader__counter-current">0</span><span class="preloader__counter-divider">&nbsp;&nbsp;/&nbsp;&nbsp;</span><span class="preloader__counter-number preloader__counter-total">100</span></div>
          <!-- - counter -->
          <!-- circle holder -->
          <div class="preloader__circle"></div>
          <!-- - circle holder -->
        </div>
        </div>
      <?php  }
      
      ?>
      <!-- - PAGE PRELOADER -->
      <!-- Loading Spinner -->
      <svg class="spinner d-lg-none" id="js-spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
        <circle class="spinner__path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
      </svg>
      <!-- - Loading Spinner -->
      <!-- Transition Curtain-->
      <!-- TRANSITION CURTAINS -->
      <!-- page curtain AJAX transition -->
      <div class="curtain transition-curtain" id="js-page-transition-curtain">
        <div class="curtain__wrapper-svg">
          <svg class="curtain-svg" viewBox="0 0 1920 1080" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <!-- Default Rectangle -->
            <path class="curtain-svg__normal" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,0 Z"></path>
            <!-- - Default Rectangle -->
            <!-- Curve Top -->
            <path class="curtain-svg__curve curtain-svg__curve_top-desktop" d="M0,300 C305.333333,100 625.333333,0 960,0 C1294.66667,0 1614.66667,100 1920,300 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,300 Z"></path>
            <path class="curtain-svg__curve curtain-svg__curve_top-mobile" d="M0,150 C305.333333,50 625.333333,0 960,0 C1294.66667,0 1614.66667,50 1920,150 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,150 Z"></path>
            <!-- - Curve Top -->
            <!-- Curve Bottom -->
            <path class="curtain-svg__curve curtain-svg__curve_bottom-desktop" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,980 1294.66667,930 960,930 C625.333333,930 305.333333,980 0,1080 L0,0 Z"></path>
            <path class="curtain-svg__curve curtain-svg__curve_bottom-mobile" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,1030 1294.66667,1005 960,1005 C625.333333,1005 305.333333,1030 0,1080 L0,0 Z"></path>
            <!-- - Curve Bottom -->
          </svg>
        </div>
      </div>
      <!-- - page curtain AJAX transition -->
      <!-- header curtain show/hide -->
      <div class="header-curtain curtain" id="js-header-curtain">
        <div class="curtain__wrapper-svg">
          <svg class="curtain-svg" viewBox="0 0 1920 1080" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <!-- Default Rectangle -->
            <path class="curtain-svg__normal" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,0 Z"></path>
            <!-- - Default Rectangle -->
            <!-- Curve Top -->
            <path class="curtain-svg__curve curtain-svg__curve_top-desktop" d="M0,300 C305.333333,100 625.333333,0 960,0 C1294.66667,0 1614.66667,100 1920,300 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,300 Z"></path>
            <path class="curtain-svg__curve curtain-svg__curve_top-mobile" d="M0,150 C305.333333,50 625.333333,0 960,0 C1294.66667,0 1614.66667,50 1920,150 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,150 Z"></path>
            <!-- - Curve Top -->
            <!-- Curve Bottom -->
            <path class="curtain-svg__curve curtain-svg__curve_bottom-desktop" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,980 1294.66667,930 960,930 C625.333333,930 305.333333,980 0,1080 L0,0 Z"></path>
            <path class="curtain-svg__curve curtain-svg__curve_bottom-mobile" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,1030 1294.66667,1005 960,1005 C625.333333,1005 305.333333,1030 0,1080 L0,0 Z"></path>
            <!-- - Curve Bottom -->
          </svg>
        </div>
      </div>
      <!-- - header curtain show/hide -->
      <!-- header curtain AJAX transition -->
      <div class="header-curtain header-curtain_transition curtain" id="js-header-curtain-transition">
        <div class="curtain__wrapper-svg">
          <svg class="curtain-svg" viewBox="0 0 1920 1080" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <!-- Default Rectangle -->
            <path class="curtain-svg__normal" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,0 Z"></path>
            <!-- - Default Rectangle -->
            <!-- Curve Top -->
            <path class="curtain-svg__curve curtain-svg__curve_top-desktop" d="M0,300 C305.333333,100 625.333333,0 960,0 C1294.66667,0 1614.66667,100 1920,300 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,300 Z"></path>
            <path class="curtain-svg__curve curtain-svg__curve_top-mobile" d="M0,150 C305.333333,50 625.333333,0 960,0 C1294.66667,0 1614.66667,50 1920,150 L1920,1080 C1614.66667,1080 1294.66667,1080 960,1080 C625.333333,1080 305.333333,1080 0,1080 L0,150 Z"></path>
            <!-- - Curve Top -->
            <!-- Curve Bottom -->
            <path class="curtain-svg__curve curtain-svg__curve_bottom-desktop" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,980 1294.66667,930 960,930 C625.333333,930 305.333333,980 0,1080 L0,0 Z"></path>
            <path class="curtain-svg__curve curtain-svg__curve_bottom-mobile" d="M0,0 C305.333333,0 625.333333,0 960,0 C1294.66667,0 1614.66667,0 1920,0 L1920,1080 C1614.66667,1030 1294.66667,1005 960,1005 C625.333333,1005 305.333333,1030 0,1080 L0,0 Z"></path>
            <!-- - Curve Bottom -->
          </svg>
        </div>
      </div>
      <!-- - header curtain AJAX transition -->
      <!-- - TRANSITION CURTAINS -->
      <!-- Cursor Follower-->
      <?php include 'inc/cursor.php'; ?>
      <!-- - Cursor Follower-->
      <!-- PAGE HEADER -->
      <?php include 'inc/navheader.php'; ?>
      <!-- - PAGE HEADER -->
      <!-- PAGE MAIN js-smooth-scroll -->
      <div class=" bg-light-1 js-smooth-scroll" id="page-wrapper" data-barba="container">
      <main data-scroll-container>
        
        <div id="header" class="content content--numbered">             
            <section class="tiles tiles-main-1 tiles--rotated section section-services section-slider-images" id="grid2">
               
                  <div class="tiles__wrap">
                    <div class="tiles__line" data-scroll="" data-scroll-speed="8" data-scroll-target="#grid2"
                      data-scroll-direction="horizontal">
                      <div class="tiles__line-img"></div>
                      <div class="tiles__line-img"></div>
                      <div class="tiles__line-img"></div>
                      <div class="tiles__line-img" style="background-image:url(https://instagram.fscl25-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/116281663_349034872773576_926091235857895696_n.jpg?_nc_ht=instagram.fscl25-1.fna.fbcdn.net&_nc_cat=100&_nc_ohc=qRodKsVKhB8AX8RA-3_&_nc_tp=24&oh=2b6790254fd94aefd4b64c55888080f7&oe=5FBEE4A8)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://instagram.fscl25-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/104341965_744442306299643_3985920865345756201_n.jpg?_nc_ht=instagram.fscl25-1.fna.fbcdn.net&_nc_cat=107&_nc_ohc=Uko1vPPl3p0AX_5zxj1&_nc_tp=24&oh=49c1b0f0493ba6a7cfd7e5352d483cd9&oe=5FBF1D26)"></div>
                      <div class="tiles__line-img" style="background-image:url(img/tiles/6.ac3cd742.jpg)"></div>
                    </div>
                    <div class="tiles__line" data-scroll="" data-scroll-speed="-8" data-scroll-target="#grid2" data-scroll-direction="horizontal">
                      <div class="tiles__line-img"></div>
                      <div class="tiles__line-img" style="background-image:url(https://instagram.fscl25-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/104176994_2437460786547011_6623881213872677808_n.jpg?_nc_ht=instagram.fscl25-1.fna.fbcdn.net&_nc_cat=111&_nc_ohc=GNCfQppFFAYAX9-SX1B&_nc_tp=24&oh=660eb20bbc1cd9a0f06628868c3171a2&oe=5FBEDDA7)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://instagram.fscl25-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/105512802_188042719314265_5596752116178146974_n.jpg?_nc_ht=instagram.fscl25-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=QwBGrmN2L8YAX_iICfz&_nc_tp=24&oh=c953de97d76d0fd75a1972351361cc03&oe=5FBCA44B)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://instagram.fscl25-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/p640x640/100924855_1067464263650826_8135148697121936210_n.jpg?_nc_ht=instagram.fscl25-1.fna.fbcdn.net&_nc_cat=106&_nc_ohc=aw-m3aJ2DK0AX9k6D7w&_nc_tp=25&oh=ad56867ab096bef4c929ab3ef6875bde&oe=5FBD7518)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://instagram.fscl25-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/p640x640/91485465_926576071096319_7072364462194370152_n.jpg?_nc_ht=instagram.fscl25-1.fna.fbcdn.net&_nc_cat=111&_nc_ohc=nuRMEyiAVqEAX84C_Cw&_nc_tp=25&oh=8e51aa28b4555a28510826b0601a9844&oe=5FBCFBE9)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://instagram.fscl25-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/85180365_217480366100824_5436046818539986827_n.jpg?_nc_ht=instagram.fscl25-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=xu9RW7bPFtkAX-a-Opp&_nc_tp=24&oh=4df5eabe24f2d94bcad13c43a5663f16&oe=5FC00D53)"></div>
                    </div>
                    <div class="tiles__line" data-scroll="" data-scroll-speed="8" data-scroll-target="#grid2" data-scroll-direction="horizontal">
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/29bf325b-dd27-4969-a87f-a8e4a2d2aad9/preview.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/0bc89560-3c27-4729-b2ce-11ba9fe536ea/teaser.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/a86166ac-13b8-49a1-97e6-7b8b759635f2/preview.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/64fd060a-fc6e-4334-b039-f6afe7a4dc30/preview.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/7499d5d4-8531-4498-bfda-8f9c444b1d16/preview.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/2178ee47-c098-47da-ad94-92f5d1ffda72/preview.jpg)"></div>
                    </div>
                    <div class="tiles__line" data-scroll="" data-scroll-speed="-8" data-scroll-target="#grid2" data-scroll-direction="horizontal">
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/80776244-ff52-42fb-9251-c482a222063f/preview.jpg)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/7b2aea27-497f-4446-baac-b324cabfbec5/preview.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/4b1c345c-ec00-4522-a09a-a585e6fb435f/preview.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/1d813c9c-ef2d-4c21-873a-d2dcf0e5f1a8/preview.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/946e5044-f28a-4cee-86ef-5fcbbc59ec4f/preview.png)"></div>
                      <div class="tiles__line-img"></div>
                    </div>
                    <div class="tiles__line" data-scroll="" data-scroll-speed="8" data-scroll-target="#grid2" data-scroll-direction="horizontal">
                      <div class="tiles__line-img"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/8a0d3a0d-7c5b-40f8-946f-39140a026927/preview.jpg)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/e9f5410d-3846-4c37-913e-acca88b7bc07/preview.png)"></div>
                      <div class="tiles__line-img" style="background-image:url(https://assets.materialup.com/uploads/eb832650-68b5-46e4-be78-a305e439c0b2/preview.jpg)"></div>
                      <div class="tiles__line-img"></div>
                      <div class="tiles__line-img"></div>
                    </div>
                  </div>
                 
            </section>  
            <section class="tiles tiles--oneline" id="grids2">
              <div class="tiles__wrap">
                <div class="tiles__line" data-scroll="" data-scroll-speed="2" 
                data-scroll-target="#grid2" data-scroll-direction="horizontal">
                  <div class="tiles__line-img" style="background-image:url(img/tiles/1.abdece96.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/2.40f30460.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/3.1e79277a.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/4.500b54f2.jpg)"></div>
                </div>
              </div>
              <h2 class="tiles__title tiles__title--right tiles__title--full tiles__title--alt" data-scroll="" data-scroll-speed="2">Web Design</h2>
            </section>
            <section class="tiles tiles--oneline" id="grid3">
              <div class="tiles__wrap">
                <div class="tiles__line" data-scroll="" data-scroll-speed="-2"
                 data-scroll-target="#grid3" data-scroll-direction="horizontal">
                  <div class="tiles__line-img" style="background-image:url(img/tiles/5.2667ae3b.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/6.aa64fc47.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/7.5ac05771.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/8.94720f30.jpg)"></div>
                </div>
              </div>
              <h2 class="tiles__title tiles__title--left tiles__title--full tiles__title--alt" data-scroll="" data-scroll-speed="2">Web Developert</h2>
            </section>
            <!--<section class="tiles tiles--oneline" id="grid4">
              <div class="tiles__wrap">
                <div class="tiles__line" data-scroll="" data-scroll-speed="2"
                 data-scroll-target="#grid4" data-scroll-direction="horizontal">
                  <div class="tiles__line-img" style="background-image:url(img/tiles/9.a23c1629.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/10.ba0c70ca.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/11.e51eb489.jpg)"></div>
                  <div class="tiles__line-img" style="background-image:url(img/tiles/12.aac53079.jpg)"></div>
                </div>
              </div>
              <h2 class="tiles__title tiles__title--right tiles__title--full tiles__title--alt" data-scroll="" data-scroll-speed="2">Apocalypse</h2>
            </section> -->  
        </div>      
      </main>  


     
        <main class="page-wrapper__content negative-bottom">
          
          <div>
            <svg xmlns="http://www.w3.org/2000/svg" class="dotted-circle" width="352" height="352" overflow="visible">
              <circle cx="176" cy="176" r="174" fill="none" stroke="#fff" stroke-width="2" stroke-miterlimit="10" stroke-dasharray="12.921,11.9271"></circle>
            </svg>
          </div>
          <!-- section MASTHEAD -->
          <section class="section section-masthead pt-large pb-medium text-center" data-arts-os-animation="data-arts-os-animation" data-background-color="var(--color-light-1)">
            <div class="section-masthead__inner container-fluid">
              <header class="row section-masthead__header justify-content-center">
                <div class="col-lg-8">
                  <div class="section-masthead__heading split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">
                    <h1 class="mt-0 mb-0 h1">Servicios   </h1>
                  </div>
                  <div class="w-100"></div>
                  <div class="section__headline mt-2 mx-auto"></div>
                </div>
              </header>
            </div>
          </section>
          <!-- - section MASTHEAD -->
        
          <!-- section SERVICES SLIDER -->
          <?php if ($sliders) { ?>
          <section class="section section-services section-slider-images">
            <div class="slider slider-services slider-images js-slider-images">
              <div class="swiper-container js-slider-images__slider" data-slides-per-view="1.4" data-slides-per-view-tablet="1.2" data-slides-per-view-mobile="1.1" data-centered-slides="true" data-autoplay-enabled="true" data-drag-mouse="true" data-drag-cursor="true" data-drag-class="slider-images_touched" data-auto-height="true">
                <div class="swiper-wrapper">
                  <?php 
                    foreach($array as $obj){
                      $id_fruta = $obj->id_fruta;
                      $titulo = $obj->titulo;
                      $imagen = $obj->imagen;
                      $description = $obj->description;
                    
                      ?>

                  <div class="swiper-slide">
                    <div class="container figure-service bg-white">
                      <div class="row no-gutters">
                        <!-- background image -->
                        <div class="col-lg-5 overflow">
                          <div class="w-100 h-100" data-swiper-parallax-zoom="0%" data-swiper-parallax="0%" style="">
                            <!-- zoom on drag container -->
                            <div class="slider__zoom-container w-100 h-100">
                              <div class="figure-service__wrapper-bg">
                                <div class="slider__bg swiper-lazy" style="background-size:contain;background-position:center;background-repeat:no-repeat" data-background="<?php   echo $imagen; ?>"></div>
                              </div>
                            </div>
                            <!-- - zoom on drag container -->
                          </div>
                        </div>
                        <!-- - background image -->
                        <!-- content -->
                        <div class="col-lg-7">
                          <div class="figure-service__content p-small">
                            <!-- header -->
                            <div class="figure-service__header">
                              <div class="figure-service__heading">
                                <h2 class="h3 mt-0 mb-0"><?php   echo $titulo; ?></h2>
                              </div>
                              <div class="figure-service__text">
                                <p class="mb-0 mt-1"><?php   echo truncate($description, 150); ?></p>
                              </div>
                            </div>
                            <!-- - header -->
                            <!-- footer -->
                            <div class="figure-service__footer d-flex justify-content-between align-items-center mt-xsmall mt-md-6">
                              <div class="figure-service__price">
                                <div class="figure-service__header small-caps mb-0-5">Empezar Ahora</div>
                                <div class="figure-service__amount h3"></div>
                              </div>
                              <div class="figure-service__wrapper-button"><a class="d-inline-block no-highlight" href="#">
                                  <div class="arrow arrow-right js-arrow" data-arts-cursor="data-arts-cursor" data-arts-cursor-hide-native="true" data-arts-cursor-scale="0" data-arts-cursor-magnetic="data-arts-cursor-magnetic">
                                    <svg class="svg-circle" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                      <circle class="circle" cx="30" cy="30" r="29" fill="none"></circle>
                                    </svg>
                                    <div class="arrow__pointer arrow-right__pointer"></div>
                                    <div class="arrow__triangle"></div>
                                  </div></a>
                              </div>
                            </div>
                            <!-- - footer -->
                          </div>
                        </div>
                        <!-- - content -->
                      </div>
                    </div>
                  </div>

                  <?php } ?>
                 
                </div>
              </div>
              <!-- slider FOOTER -->
              <div class="container slider-services__footer mt-1 mt-md-2 mb-1">
                <div class="row justify-content-between align-items-center">
                  <!-- slider COUNTER (current) -->
                  <div class="col-auto">
                    <div class="slider__counter slider__counter_mini">
                      <div class="js-slider__counter-current swiper-container">
                        <div class="swiper-wrapper"></div>
                      </div>
                    </div>
                  </div>
                  <!-- - slider COUNTER (current) -->
                  <!-- slider DOTS -->
                  <div class="col-auto">
                    <div class="slider__dots js-slider__dots">
                      <div class="slider__dot slider__dot_active">
                        <svg class="svg-circle" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <circle class="circle" cx="30" cy="30" r="29" fill="none"></circle>
                        </svg>
                      </div>
                      <div class="slider__dot">
                        <svg class="svg-circle" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <circle class="circle" cx="30" cy="30" r="29" fill="none"></circle>
                        </svg>
                      </div>
                      <div class="slider__dot">
                        <svg class="svg-circle" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <circle class="circle" cx="30" cy="30" r="29" fill="none"></circle>
                        </svg>
                      </div>
                      <div class="slider__dot">
                        <svg class="svg-circle" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <circle class="circle" cx="30" cy="30" r="29" fill="none"></circle>
                        </svg>
                      </div>
                    </div>
                  </div>
                  <!-- - slider DOTS -->
                  <!-- slider COUNTER (total) -->
                  <div class="col-auto">
                    <div class="slider__total slider__total_mini js-slider__counter-total">I</div>
                  </div>
                  <!-- - slider COUNTER (total) -->
                </div>
              </div>
              <!-- - slider FOOTER -->
            </div>
          </section>
          <?php } ?>
          <!-- - section SERVICES SLIDER -->
              
      
          <!-- section CONTENT #1 -->
          <?php include 'inc/home/who.php'; ?>
          <?php if ($team){ ?>
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-8">
                <section class="section section-content clearfix pt-medium pb-small text-center" data-arts-os-animation="data-arts-os-animation">
                  <div class="section-content__inner">
                    <div class="w-100"></div>
                    <div class="section__headline mb-1 mb-md-2 mx-auto"></div>
                    <div class="w-100"></div>
                    <div class="section-content__heading split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">
                      <h2>Our Team</h2>
                    </div>
                    <div class="w-100"></div>
                    <div class="section-content__text split-text js-split-text mt-1" data-split-text-type="lines" data-split-text-set="lines">
                      <p>Too horrible consider followed may differed age. An rest if more five mr of. Age just her rank met down way. Attended required so in cheerful an.</p>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          
          <!-- - section CONTENT #1 -->
          <!-- section TEAM -->
          <section class="section section-team section-grid text-center mb-medium" data-arts-os-animation="data-arts-os-animation" data-grid-columns="3" data-grid-columns-tablet="2" data-grid-columns-mobile="1">
            <div class="container">
              <div class="row row-gutters">
                 <div class="col-lg-6 col-sm-6 col-gutters">
                  <div class="section-grid__item">
                    <div class="figure-member figure-member_has-social">
                      <!-- image -->
                      <div class="figure-member__avatar">
                        <div class="wrapper-distorsion"></div>
                        <div class="hero-title"><h1 class="letter">OFFBEAT</h1></div>
                      </div>
                      <!-- - image -->
                      <div class="figure-member__headline mt-1"></div>
                      <!-- content -->
                      <div class="figure-member__footer mt-0-5">
                        <div class="figure-member__name h4">Hattie Barber</div>
                        <div class="figure-member__position small-caps mt-1">Art Director</div>
                        <div class="figure-member__social">
                          <ul class="social">
                            <li class="social__item"><a class="fa fa-facebook-f" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-twitter" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-instagram" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-behance" href="#"></a></li>
                          </ul>
                        </div>
                      </div>
                      <!-- - content -->
                    </div>
                  </div>
                 </div>
                 <div class="col-lg-6 col-sm-6 col-gutters">
                 <div class="section-grid__item">
                    <div class="figure-member figure-member_has-social">
                      <!-- image -->
                      <div class="figure-member__avatar">
                        <div class="wrapper-distorsion-2"></div>
                        <div class="hero-title"><h1 class="letter">OFFBEAT</h1></div>
                      </div>
                      <!-- - image -->
                      <div class="figure-member__headline mt-1"></div>
                      <!-- content -->
                      <div class="figure-member__footer mt-0-5">
                        <div class="figure-member__name h4">Hattie Barber</div>
                        <div class="figure-member__position small-caps mt-1">Art Director</div>
                        <div class="figure-member__social">
                          <ul class="social">
                            <li class="social__item"><a class="fa fa-facebook-f" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-twitter" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-instagram" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-behance" href="#"></a></li>
                          </ul>
                        </div>
                      </div>
                      <!-- - content -->
                    </div>
                  </div>
                 </div>
                
                
                <div class="col-lg-4 col-sm-6 col-gutters">
                  <div class="section-grid__item">
                    <div class="figure-member figure-member_has-social">
                      <!-- image -->
                      <div class="figure-member__avatar">
                        <div class="lazy"><img src="#" data-src="img/assets/sectionTeam/member-4.jpg" width="680" height="1020" alt></div>
                      </div>
                      <!-- - image -->
                      <div class="figure-member__headline mt-1"></div>
                      <!-- content -->
                      <div class="figure-member__footer mt-0-5">
                        <div class="figure-member__name h4">Rosetta Miles</div>
                        <div class="figure-member__position small-caps mt-1">Frontend Developer</div>
                        <div class="figure-member__social">
                          <ul class="social">
                            <li class="social__item"><a class="fa fa-facebook-f" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-twitter" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-instagram" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-behance" href="#"></a></li>
                          </ul>
                        </div>
                      </div>
                      <!-- - content -->
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-gutters">
                  <div class="section-grid__item">
                    <div class="figure-member figure-member_has-social">
                      <!-- image -->
                      <div class="figure-member__avatar">
                        <div class="lazy"><img src="#" data-src="img/assets/sectionTeam/member-5.jpg" width="680" height="1020" alt></div>
                      </div>
                      <!-- - image -->
                      <div class="figure-member__headline mt-1"></div>
                      <!-- content -->
                      <div class="figure-member__footer mt-0-5">
                        <div class="figure-member__name h4">David Parker</div>
                        <div class="figure-member__position small-caps mt-1">Product Manager</div>
                        <div class="figure-member__social">
                          <ul class="social">
                            <li class="social__item"><a class="fa fa-facebook-f" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-twitter" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-instagram" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-behance" href="#"></a></li>
                          </ul>
                        </div>
                      </div>
                      <!-- - content -->
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-gutters">
                  <div class="section-grid__item">
                    <div class="figure-member figure-member_has-social">
                      <!-- image -->
                      <div class="figure-member__avatar">
                        <div class="lazy"><img src="#" data-src="img/assets/sectionTeam/member-6.jpg" width="680" height="1020" alt></div>
                      </div>
                      <!-- - image -->
                      <div class="figure-member__headline mt-1"></div>
                      <!-- content -->
                      <div class="figure-member__footer mt-0-5">
                        <div class="figure-member__name h4">Mark Newman</div>
                        <div class="figure-member__position small-caps mt-1">Backend Developer</div>
                        <div class="figure-member__social">
                          <ul class="social">
                            <li class="social__item"><a class="fa fa-facebook-f" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-twitter" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-instagram" href="#"></a></li>
                            <li class="social__item"><a class="fa fa-behance" href="#"></a></li>
                          </ul>
                        </div>
                      </div>
                      <!-- - content -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <?php  } ?>
          <?php include 'inc/home/porfolio.php'; ?>
          <!-- -  section TEAM -->
          <!-- section CTA -->
          <section class="section section-cta section-content pt-medium pb-medium pt-md-4 pb-md-4 bg-dark-3" data-arts-os-animation="data-arts-os-animation" data-arts-theme-text="light">
            <div class="container">
              <div class="row justify-content-between align-items-center">
                <div class="col-lg-8">
                  <div class="section-cta__heading split-text js-split-text" data-split-text-type="lines,words" data-split-text-set="words">
                    <h2 class="h3 mt-0 mt-md-1 mb-1">Quieres trbajar con nosotros?</h2>
                  </div>
                </div>
                <div class="col-lg-auto">
                  <div class="section-content__button"><a class="button button_bordered button_white" data-hover="Get in Touch" href="#"><span class="button__label-hover">Get in Touch</span></a>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!-- - section CTA -->
          <!-- aside COUNTERS -->
          <aside class="aside aside-counters section aside-counters_4 pt-small pb-medium text-center">
            <div class="container">
              <div class="row justify-content-center">
                <!-- counter -->
                <div class="aside-counters__wrapper-item col-lg-3 col-6 mt-small">
                  <div class="counter js-counter" data-counter-start="0" data-counter-target="139" data-counter-duration="4">
                    <!-- animated number -->
                    <div class="counter__number js-counter__number">139</div>
                    <!-- - animated number -->
                    <div class="counter__label">Tiro de modelos</div>
                  </div>
                </div>
                <!-- - counter -->
                <!-- counter -->
                <div class="aside-counters__wrapper-item col-lg-3 col-6 mt-small">
                  <div class="counter js-counter" data-counter-start="0" data-counter-target="52" data-counter-duration="4">
                    <!-- animated number -->
                    <div class="counter__number js-counter__number">52</div>
                    <!-- - animated number -->
                    <div class="counter__label">Clientes felices</div>
                  </div>
                </div>
                <!-- - counter -->
                <!-- counter -->
                <div class="aside-counters__wrapper-item col-lg-3 col-6 mt-small">
                  <div class="counter js-counter" data-counter-start="0" data-counter-target="632" data-counter-duration="4">
                    <!-- animated number -->
                    <div class="counter__number js-counter__number">632</div>
                    <!-- - animated number -->
                    <div class="counter__label">Puestas de sol capturadas</div>
                  </div>
                </div>
                <!-- - counter -->
                <!-- counter -->
                <div class="aside-counters__wrapper-item col-lg-3 col-6 mt-small">
                  <div class="counter js-counter" data-counter-start="0" data-counter-target="245" data-counter-duration="4">
                    <!-- animated number -->
                    <div class="counter__number js-counter__number">245</div>
                    <!-- - animated number -->
                    <div class="counter__label">Café borracho</div>
                  </div>
                </div>
                <!-- - counter -->
              </div>
            </div>
          </aside>
          <!-- - aside COUNTERS -->
        </main>
        <!-- PAGE FOOTER -->
        <?php include 'inc/footer-html.php'; ?>
        <!-- - PAGE FOOTER -->
      </div>

      
      <!-- - PAGE MAIN -->
    </div>
    <canvas id="js-webgl"></canvas>
    <!-- PhotoSwipe -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true" data-arts-theme-text="light">
      <!-- background -->
      <div class="pswp__bg"></div>
      <!-- - background -->
      <!-- slider wrapper -->
      <div class="pswp__scroll-wrap">
        <!-- slides holder (don't modify)-->
        <div class="pswp__container">
          <div class="pswp__item">
            <div class="pswp__img pswp__img--placeholder"></div>
          </div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <!-- - slides holder (don't modify)-->
        <!-- UI -->
        <div class="pswp__ui pswp__ui--hidden">
          <!-- top bar -->
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)" data-arts-cursor="data-arts-cursor" data-arts-cursor-scale="1.2" data-arts-cursor-magnetic="data-arts-cursor-magnetic" data-arts-cursor-hide-native="true"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen" data-arts-cursor="data-arts-cursor" data-arts-cursor-scale="1.2" data-arts-cursor-magnetic="data-arts-cursor-magnetic" data-arts-cursor-hide-native="true"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <!-- - top bar -->
          <!-- left arrow -->
          <div class="pswp__button pswp__button--arrow--left">
            <div class="arrow arrow-left js-arrow" data-arts-cursor="data-arts-cursor" data-arts-cursor-hide-native="true" data-arts-cursor-scale="0" data-arts-cursor-magnetic="data-arts-cursor-magnetic">
              <svg class="svg-circle" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <circle class="circle" cx="30" cy="30" r="29" fill="none"></circle>
              </svg>
              <div class="arrow__pointer arrow-left__pointer"></div>
              <div class="arrow__triangle"></div>
            </div>
          </div>
          <!-- - left arrow -->
          <!-- right arrow -->
          <div class="pswp__button pswp__button--arrow--right">
            <div class="arrow arrow-right js-arrow" data-arts-cursor="data-arts-cursor" data-arts-cursor-hide-native="true" data-arts-cursor-scale="0" data-arts-cursor-magnetic="data-arts-cursor-magnetic">
              <svg class="svg-circle" viewBox="0 0 60 60" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <circle class="circle" cx="30" cy="30" r="29" fill="none"></circle>
              </svg>
              <div class="arrow__pointer arrow-right__pointer"></div>
              <div class="arrow__triangle"></div>
            </div>
          </div>
          <!-- - right arrow -->
          <!-- slide caption holder (don't modify) -->
          <div class="pswp__caption">
            <div class="pswp__caption__center text-center"></div>
          </div>
          <!-- - slide caption holder (don't modify) -->
        </div>
        <!-- - UI -->
      </div>
      <!-- slider wrapper -->
    </div>
    <!-- - PhotoSwipe -->
    <!-- List Hover Shaders -->
    <?php include 'inc/footer.php'; ?>
  </body>

<!-- Mirrored from artemsemkin.com/rhye/html/page-inner-services-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 05 Jun 2020 03:12:04 GMT -->
</html>