const path = require('path');
const fs = require('fs');

const livereload = require('livereload');
const lrserver = livereload.createServer();

const compiled = path.join();
lrserver.watch( compiled ); 
console.log( compiled)
const connect = require('connect');
const stat = require('serve-static');

const server = connect();
server.use( stat( compiled ));

server.listen( 3033 );

console.log( 'Dev server on localhost:3033' );